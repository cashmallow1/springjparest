package com.spr.service;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.persistence.EntityManagerFactory;
import javax.persistence.PersistenceUnit;
import javax.persistence.criteria.CriteriaBuilder;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import javax.persistence.metamodel.EntityType;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.domain.Specification;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;


import com.spr.repository.EmployeeRepository;
import com.spr.model.Employee;

@Service
public class EmployeeServiceImpl implements EmployeeService {

	@PersistenceUnit
	private	EntityManagerFactory	entityManagerFactory;
	
	@Autowired
	private	EmployeeRepository	empRepo;
	
	public void addEmployee(Employee emp) {		
		empRepo.save(emp);		
	}

	public void updateEmployee(Employee emp) {
		empRepo.save(emp);
	}

	public Employee getEmployee(int id) {
		return empRepo.findOne(id);
	}

	public void deleteEmployee(int id) {
		empRepo.delete(id);
	}
	
	
	@Transactional
	public List<Employee> allEmployees () {			
		return empRepo.findAll( ); 		
	}
		
	@Transactional
	public	List<Employee>	listEmployeesOverAge(int age){
		System.out.print("AGE : " + age );

		//return empRepo.findByAgeOrderByEmpNameAsc(age);
		return	empRepo.findOverAge(age);
	}
	
	@Transactional
	public	List<Employee>	listEmployeesSameAge(int age){
		System.out.print("AGE : " + age );

		return empRepo.findByAgeOrderByEmpNameAsc(age);		
	}
	
	@Transactional
	public Map<String, Object>	listEmployeesPageable( int pg_index, int pg_size) {
		Page<Employee>	page = empRepo.findAll( new PageRequest( pg_index, pg_size, new Sort(Sort.Direction.DESC, "empName")) ); //
		
		HashMap<String, Object>	map = new HashMap();
		map.put("totalRecords",	empRepo.count() );
		map.put("totalPages",	page.getTotalPages() );
		map.put("curPage", 		page.getNumber());
		map.put("employees",	page.getContent());
				
		return map;		
	}
	
	
	@Transactional
	public	List<Employee>	listEmployeesWithCriteria(String like_name, int under_age) {
		return empRepo.findAll( nameLikeUnderAge(like_name, under_age));
	}	
	
	public	Specification<Employee>	nameLikeUnderAge( final String like_name,final int age ){
		return new Specification<Employee>() {
			@Override
			public Predicate toPredicate(Root<Employee> root, CriteriaQuery<?> query, CriteriaBuilder cb) {
				return cb.and( cb.like( root.<String>get("empName"), like_name + "%"),
								cb.le(root.<Number>get("age"), age));
								
				
			}
		};
	}
}
