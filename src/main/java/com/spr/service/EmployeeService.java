package com.spr.service;

import java.util.List;
import java.util.Map;

import com.spr.model.Employee;

public interface EmployeeService {
	public	void			addEmployee		(Employee emp);
	public	void 			updateEmployee	(Employee emp);
	public	Employee 		getEmployee		(int id);
	public	void 			deleteEmployee	(int id);
	
	public	List<Employee>	allEmployees	();
	public	List<Employee>	listEmployeesOverAge(int age);
	public	List<Employee>	listEmployeesSameAge(int age);
	public	Map				listEmployeesPageable	( int pg_index, int pg_size);
	public	List<Employee>	listEmployeesWithCriteria(String like_name, int under_age);
}
