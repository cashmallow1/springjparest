package com.spr.repository;

import java.util.List;

import org.springframework.data.jpa.domain.Specification;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import com.spr.model.Employee;

//@Repository
public interface EmployeeRepository extends JpaRepository <Employee, Integer > /*, JpaSpecificationExecutor<Todo>*/ {		
	
	@Query("SELECT t FROM Employee t WHERE t.age > :age ")
    public List<Employee> findOverAge( @Param("age") int age );
	
	List<Employee> findByAgeOrderByEmpNameAsc(int age);		// Wow It works, even if there is no method implementation.
	List<Employee> findAll(Specification<Employee> spec);
}